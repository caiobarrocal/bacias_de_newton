import java.awt.Color;
import java.util.Arrays;

public class NewtonBasins 
{
	
	public static void draw(HolomorphicFunction f, int maxI, double x, double y, double xsize, double ysize, int M, int N)
	{
		Complex cie = new Complex((x - xsize/2),(y - ysize/2)); //Variável para o canto inferior esquerdo do retângulo
		Complex csd = new Complex((x + xsize/2),(y + ysize/2)); //Variável para o canto superior direito do retângulo
		Complex cid = new Complex((x + xsize/2),(y - ysize/2)); //Variável para o canto inferior direito do retângulo
		Complex cse = new Complex((x - xsize/2),(y + ysize/2)); //Variável para o canto superior esquerdo do retângulo

		Complex root;
		Complex[] roots = new Complex[1000]; // Vetor que guardará as raízes obtidas pelo método de Newton
		Color[] colors = new Color[1000]; // Vetor paralelo ao r que guardará as cores aplicadas a cada raíz de r
		int cIndex;
		Color color;


		Picture pic = new Picture(M, N);

		for(int i = 0; i < M; i++)
		{
			for(int j = 0; j < N; j++)
			{
				Counter C = new Counter("iteracoesNewton"); // Construindo novo Counter

				double x0 = cie.re + i*xsize/M;
				double y0 = cie.im + j*ysize/N;
				Complex z0 = new Complex(x0, y0);
				
				root = Newton.findRoot(f, z0, C); // Método onde ocorre a aplicação do 'Método de Newton'
				
				if(root == null) {color = Color.BLACK;} // Atribui cor Preta ao pixel quando não há convergência no Método de Newton
				else {
					
					cIndex = ColorIndex.findColorIndex(root, roots); // Método findColorIndex da classe ColorIndex recebe a raíz obtida e o vetor de raízes.
					
					// Resize?? 
					if(cIndex == -1) // Checa se o vetor precisa ter seu tamanho aumentado conforme instrução passada pela findColorIndex!
					{
						roots = java.util.Arrays.copyOf(roots, roots.length*2); //Vetor redimensionado
						j--;
						continue; // Repete iteração passada!
					}

					//Checa se o vetor de raízes foi aumentado, caso sim, o vetor paralelo de cores também o é.
					if(roots.length != colors.length) colors = java.util.Arrays.copyOf(colors, roots.length); 
					
					color = FindColor.findColor(C.tally(), maxI, cIndex, colors); //Chama o método estático findColor para retornar cor!
				}
				pic.set(i, j, color); 
			}
		}
		pic.show();
	}

}