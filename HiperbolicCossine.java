public class HiperbolicCossine implements HolomorphicFunction
{

	public static Complex exponential(Complex z)
	{
		double x = z.re();
		double y = z.im();
		double ex = Math.exp(x);

		//retorna f(z) = f(x,y) = e^x(cosy + iseny)
		Complex exp = new Complex(ex*Math.cos(y), ex*Math.sin(y));
		return exp;

	}


	// f(z) = cosh(z) - 1 = cos(iz) - 1
	public Complex eval(Complex z)
	{
		Complex i = new Complex (0.0, 1.0);
		Complex one = new Complex(1.0, 0.0);
		Complex valor = ((z.times(i)).cos()).minus(one);
		return valor;
	}
	// f'(z) = senh(i) = 1/csch(z);
	public Complex diff(Complex z)
	{
		Complex one = new Complex (1.0, 0.0);
		Complex i = new Complex (0.0, 1.0);

		// csch(z) = 2e^z/(-1 + e^2z)
		Complex cschDividendo = exponential(z).times(2);

		Complex cschDivisor = (exponential(z.times(2))).minus(one);

		Complex csch = cschDividendo.divides(cschDivisor);

		Complex derivada = one.divides(csch);
		
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new HiperbolicCossine();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}