import java.awt.Color;

//A classe FindColor é destinada a estabelecer cores conforme os índices obtidos através da ColorIndex

public class FindColor
{
	// Retorna uma cor aleatória utilizando distribuição uniforme
	public static Color randomColor()
	{
		int r = (int) StdRandom.uniform(1.0, 255.0);
		int g = (int) StdRandom.uniform(1.0, 255.0);
		int b = (int) StdRandom.uniform(1.0, 255.0);
		Color color = new Color(r,g,b);
		return color;

	}

	// Aplica a classe propriamente dita. Este método verifica se o índice passado pela classe ColorIndex (com nome de cIndex) quando aplicado ao 
	// vetor paralelo colors implica em uma cor anteriormente designada (significando a mesma raíz, portanto a mesma cor) ou se resulta em um 
	// null do vetor. Caso seja null, é estabelecida uma cor aleatória através do método randomColor. Este método também escurece a cor conforme
	// as iterações registradas no Counter
	public static Color findColor(int iteracoes, int maxI, int cIndex, Color[] colors)
	{
		double proporcao;
		double red, green, blue;
		int redI, greenI, blueI;
		Color color;

		if(colors[cIndex] != null) color = colors[cIndex];
		else
		{
			colors[cIndex] = randomColor();
			color = colors[cIndex];
		}
		
		// Considerar iterações do Counter para escurecer ou não a cor
		if(iteracoes >= maxI) return color;
		else
		{
			proporcao = (double) maxI/iteracoes;

			red = color.getRed()/proporcao;
			redI = (int) red;
			green = color.getGreen()/proporcao;
			greenI = (int) green;
			blue = color.getBlue()/proporcao;
			blueI = (int) blue;

			color = new Color(redI,greenI,blueI);
			return color;
		}

	}
}