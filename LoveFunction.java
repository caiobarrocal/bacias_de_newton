// Love Function foi nomeada por desenhar formas que lembram (vagamente rs) um coração :)

public class LoveFunction implements HolomorphicFunction
{
	//f(z) = (0.5z^4 + 2z^2 - 0.9z -1)/(-0.7z^2 - 0.2z +1)
	public Complex eval(Complex z)
	{
		Complex one = new Complex(1.0,0.0);

		//Separando Dividendo

		//Primeiro Termo
		Complex zp = (z.times(z).times(z).times(z)).times(0.5);

		//Segundo Termo
		Complex zs = (z.times(z)).times(2);

		//Terceiro Termo
		Complex zt = z.times(0.9);

		//Dividendo 
		Complex dividendo = zp.plus(zs).minus(zt).minus(one);

		//Separando Divisor

		//Primeiro Termo
		Complex zp2 = (z.times(z)).times(-0.7);

		//Segundo Termo
		Complex zs2 = z.times(0.2);

		//Divisor
		Complex divisor = zp2.minus(zs2).plus(one);


		Complex valor = dividendo.divides(divisor);
		return valor;
	}

	//f'(z) = (-1.42857z^5 - 0.612245z^4 + 4.08163z^3 - 2.10204z^2 + 5.30612z - 2.2449)/(-z^2 - 0.285714z + 1.42857)^2
	public Complex diff(Complex z)
	{

		//Separando dividendo

		//Primeiro Termo
		Complex zp = (z.times(z).times(z).times(z).times(z)).times(-1.42857);

		//Segundo Termo
		Complex zs = (z.times(z).times(z).times(z)).times(0.612245);

		//Terceiro Termo
		Complex zt = (z.times(z).times(z)).times(4.08163);

		//Quarto Termo
		Complex zq = (z.times(z)).times(2.10204);

		//Quinto Termo
		Complex zqui = z.times(5.30612);

		//Sexto Termo
		Complex zsex = new Complex(2.2449, 0.0);

		//Dividendo
		Complex dividendo = zp.minus(zs).plus(zt).minus(zq).plus(zqui).minus(zsex);

		//Separando Divisor

		//Primeiro Termo
		Complex zp2 = (z.times(z)).times(-1);

		//Segundo Termo
		Complex zs2 = z.times(0.285714);

		//Terceiro Termo
		Complex zt2 = new Complex(1.42857, 0.0);

		//Raíz do Divisor
		Complex divisorSqrt = zp2.minus(zs2).plus(zt2);

		//Divisor
		Complex divisor = divisorSqrt.times(divisorSqrt);

		Complex derivada = dividendo.divides(divisor);
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new LoveFunction();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}