public class NewSin implements HolomorphicFunction
{
	// f(z) = sen(z)
	public Complex eval(Complex z)
	{
		Complex valor = z.sin();
		return valor;
	}
	// f'(z) = cos(z)
	public Complex diff(Complex z)
	{
		Complex derivada = z.cos();
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new NewSin();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}