// A classe Newton calcula as raízes através de convergência utilizando o método de Newton!
//Também é passada a essa classe, em forma de variável global, o valor maxI passado como argumento de NewtonBasins.draw
//para que o iterador pare quando atingir o limite estabelecido!

public class Newton 
{	
	public static Complex findRoot(HolomorphicFunction f, Complex z0, Counter N)
	{	
		double epsilon = 0.000001;
		Complex zn = new Complex(0.0,0.0);
		Complex valor;		
		Complex derivada;
		int ok = 0;

				
		while((f.eval(z0).abs() > epsilon || ok==0) && N.tally() < 300) 
		{	
			if (ok!=0) z0 = zn; //Evita que seja aplicado na primeira iteração
			valor = f.eval(z0);
			derivada = f.diff(z0);
			zn = z0.minus((valor.divides(derivada)));
			N.increment(); //Incrementa contador
			ok = 1; // Apenas marca que a primeira iteração já foi
			if(N.tally() == 300) return null; // Retorna null se não converge!
		}	
		return z0;
	}	
}	