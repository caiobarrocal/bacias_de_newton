import java.util.Arrays;

// A classe ColorIndex recebe a aproximação de raíz obtida por Newton e a compara com outras aproximações presentes no vetor roots.
// Caso a raíz passada tenha variação menor que epsilon quando comparada ao valor já no vetor, assume-se que são a mesma raíz e portanto
// a função devolve o mesmo índice. Caso a raíz passada não tenha convergência nenhuma no vetor, o loop atinge um espaço null no qual um espaço no
//vetor é destinado à nova raíz. O índice obtido nessa classe será futuramente usado na classe FindColor aplicado a um vetor paralelo.

public class ColorIndex 
{
	public static int findColorIndex (Complex z0, Complex[] roots)
	{
		int ok = 0, ind = 0;
		double epsilon = 0.1;

		for(int i = 0; i < roots.length && ok == 0; i++) 
		{
			if(i == roots.length-1) return -1; //Retorna -1 para que o array seja aumentado quando necessário!

			if (roots[i] != null) {
				if ((z0.minus(roots[i])).abs() < 2*epsilon || roots[i].equals(z0)) return i;
			}
			if (roots[i] == null) 
			{
				roots[i] = z0;
				ok = 1; //Marca que foi atribuida uma nova raíz ao vetor roots, portanto não é mais necessário iterar.
				ind = i;
			}
		}
		return ind;
	}	
}