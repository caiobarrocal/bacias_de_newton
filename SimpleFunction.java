public class SimpleFunction implements HolomorphicFunction
{
	// f(z) = z^2 + 1
	public Complex eval(Complex z)
	{
		Complex one = new Complex(1.0,0.0);
		Complex valor = (z.times(z)).plus(one);
		return valor;
	}
	// f'(z) = 2z
	public Complex diff(Complex z)
	{
		Complex derivada = z.times(2);
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new SimpleFunction();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}