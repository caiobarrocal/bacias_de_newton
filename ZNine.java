public class ZNine implements HolomorphicFunction
{
	// f(z) = z^9 - 1
	public Complex eval(Complex z)
	{
		Complex one = new Complex(1.0,0.0);
		Complex z9 = z.times(z).times(z).times(z).times(z).times(z).times(z).times(z).times(z);
		Complex valor = z9.minus(one);
		return valor;
	}
	// f'(z) = 9z^8
	public Complex diff(Complex z)
	{
		Complex z8 = z.times(z).times(z).times(z).times(z).times(z).times(z).times(z);
		Complex derivada = z8.times(9);
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new ZNine();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}