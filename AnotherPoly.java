public class AnotherPoly implements HolomorphicFunction 
{
	// retorna f(z)=2z^3 - 2z + 2
	public Complex eval(Complex z)
	{
		Complex two = new Complex(2.0,0.0);

		//Desenvolvendo primeiro termo
		Complex z3 = z.times(z).times(z);
		Complex twoz3 = z3.times(2); // Primeiro termo

		//Segundo termo
		Complex twoz = z.times(2);


		Complex valor = twoz3.minus(twoz).plus(two);
		return valor;
	}
	// retorna f'(z) = 6z^2 - 2
	public Complex diff(Complex z)
 	{
 		Complex two = new Complex(2.0,0.0);

		Complex z2 = z.times(z);
		Complex z26 = z2.times(6);
		Complex derivada = z26.minus(two);
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new AnotherPoly();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}
