public class ZFive implements HolomorphicFunction 
{
	// retorna f(z)=z^5 - 1
	public Complex eval(Complex z)
	{
		Complex one = new Complex(1.0,0.0);
		Complex z5 = z.times(z).times(z).times(z).times(z);

		Complex valor = z5.minus(one);
		return valor;
	}
	// retorna f'(z)=5z^4
	public Complex diff(Complex z)
	{
		Complex z4 = z.times(z).times(z).times(z);
		Complex derivada = z4.times(5);
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new ZFive();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}