public class ZCube implements HolomorphicFunction 
{
	// retorna f(z)=z^3
	public Complex eval(Complex z)
	{
		Complex z3 = z.times(z).times(z);

		Complex valor = z3;
		return valor;
	}
	// retorna f'(z)=3z^2
	public Complex diff(Complex z)
	{
		Complex z2 = z.times(z);
		Complex derivada = z2.times(3);
		return derivada;
	}

	public static void main(String[] args) 
	{
		int maxI = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = Double.parseDouble(args[2]);
		double xsize = Double.parseDouble(args[3]);
		double ysize = Double.parseDouble(args[4]);
		int M = Integer.parseInt(args[5]);
		int N = Integer.parseInt(args[6]);

		HolomorphicFunction f = new ZCube();

		NewtonBasins.draw(f, maxI, x, y, xsize, ysize, M, N);
	}
}